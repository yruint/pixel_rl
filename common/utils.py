import torch
import torch.nn as nn
import numpy as np
import gym
import os
import os.path as osp
import json
import yaml
from collections import deque
import random
import torch.nn.functional as F
import time
from skimage.util.shape import view_as_windows
import dmc2gym
# from distracing_control import suite
from common import logger, suite, wrappers, logx


def make_env(domain_name, task_name, seed, video_alpha, args):
    if video_alpha <= 0 or video_alpha > 1:
        background_kwargs = None
    else:
        background_kwargs = dict(video_alpha=video_alpha)

    image_size = args.pre_image_size if args.crop else args.image_size

    if video_alpha:
        env = suite.load(
            domain_name, task_name,
            background_dataset_path="/opt/rl/dataset/DAVIS/JPEGImages/480p",
            background_kwargs=background_kwargs,
            visualize_reward=False,
        )
        env = wrappers.DMCWrapper(
            env,
            seed=seed,
            from_pixels=(args.encoder_type == 'pixel'),
            height=image_size,
            width=image_size,
            frame_skip=args.action_repeat
        )
    else:
        env = dmc2gym.make(
            domain_name=domain_name,
            task_name=task_name,
            seed=seed,
            visualize_reward=False,
            from_pixels=(args.encoder_type == 'pixel'),
            height=image_size,
            width=image_size,
            frame_skip=args.action_repeat
        )
    return env


def read_config(args, config_dir):
    com_config_dir = os.path.join(config_dir, 'common.yaml')
    with open(com_config_dir) as f:
        config = yaml.load(f, Loader=yaml.SafeLoader)
    # args.eval_freq = config['eval_freq'] // args.action_repeat
    
    algo_config_dir = os.path.join(config_dir, f'{args.alg}.yaml')
    with open(algo_config_dir) as f:
        alg_config = yaml.load(f, Loader=yaml.SafeLoader)
    config.update(alg_config)

    if args.sparse:
        extr_config_dir = os.path.join(config_dir, 'extractor.yaml')
        with open(extr_config_dir) as f:
            extr_config = yaml.load(f, Loader=yaml.SafeLoader)
        config.update(extr_config)
    
    if args.cpc:
        cpc_config_dir = os.path.join(config_dir, 'cpc.yaml')
        with open(cpc_config_dir) as f:
            cpc_config = yaml.load(f, Loader=yaml.SafeLoader)
        config.update(cpc_config)

    args_dict = vars(args)
    config.update(args_dict)
    return config


def set_extr_config(device, config):
    extr_config = dict(
        encoder_type=config['encoder_type'],
        extr_latent_dim=config['extr_latent_dim'],
        num_layers=config['num_layers'],
        num_filters=config['num_filters'],
        device=device,
        sparse=config['sparse'], cpc=config['cpc'], ln=config['ln']
    )
    if config['sparse']:
        extr_config.update(dict(
            extr_lr=config['extr_lr'], extr_tau=config['extr_tau'], extr_update_freq=config['extr_update_freq']
        ))
    if config['cpc']:
        extr_config.update(dict(
            cpc_lr=config['cpc_lr'], cpc_tau=config['cpc_tau'], cpc_update_freq=config['cpc_update_freq']
        ))
    return extr_config


def update_linear_schedule(optimizer, lr):
    """Decreases the learning rate linearly"""
    for param_group in optimizer.param_groups:
        param_group['lr'] = lr


def soft_update_params(net, target_net, tau):
    for param, target_param in zip(net.parameters(), target_net.parameters()):
        target_param.data.copy_(
            tau * param.data + (1 - tau) * target_param.data
        )


def set_seed_everywhere(seed):
    torch.manual_seed(seed)
    if torch.cuda.is_available():
        torch.cuda.manual_seed_all(seed)
    np.random.seed(seed)
    random.seed(seed)


def update_params(optim, loss, retain_graph=False, grad_cliping=False, networks=None):
    if not isinstance(optim, dict):
        optim = dict(optimizer=optim)
    for opt in optim:
        optim[opt].zero_grad()
    loss.backward(retain_graph=retain_graph)
    # Clip norms of gradients to stebilize training.
    if grad_cliping:
        try:
            for net in networks:
                nn.utils.clip_grad_norm_(net.parameters(), grad_cliping)
        except:
            nn.utils.clip_grad_norm_(networks.parameters(), grad_cliping)
    for opt in optim:
        optim[opt].step()


def log_sum_exp(value, dim):
    m, _ = torch.max(value, dim=dim, keepdim=True)
    value_norm = value - m
    m = m.squeeze(dim)
    return m+torch.log(torch.sum(torch.exp(value_norm), dim=dim))


# https://github.com/xtma/dsac/blob/master/rlkit/torch/dsac/dsac.py
def calc_presum_tau(batch_size, num_quantiles, device, alg):
    assert batch_size != 0 and num_quantiles != 0 and device != None and alg != 'fqf'
    if 'qr' in alg:
        presum_tau = torch.zeros(batch_size, num_quantiles, device=device) + 1. / num_quantiles
    elif 'iqn' in alg:
        presum_tau = torch.rand(batch_size, num_quantiles, device=device) + 0.1
        presum_tau /= presum_tau.sum(dim=-1, keepdims=True)
    else:
        raise NotImplementedError('get_tau must implemented under qr or iqn without presum_tau')
    return presum_tau


# https://github.com/xtma/dsac/blob/master/rlkit/torch/dsac/dsac.py
def get_tau(batch_size=0, num_quantiles=0, device=None, alg='qr', presum_tau=None):
    if presum_tau is None:
        presum_tau = calc_presum_tau(batch_size, num_quantiles, device, alg)
    tau = torch.cumsum(presum_tau, dim=1)
    with torch.no_grad():
        tau_hat = torch.zeros_like(tau)
        tau_hat[:, 0:1] = tau[:, 0:1] / 2.
        tau_hat[:, 1:] = (tau[:, 1:] + tau[:, :-1]) / 2.
    return tau, tau_hat, presum_tau


def qr_loss(inputs, targets, tau, weight):
    inputs = inputs.unsqueeze(-1)
    targets = targets.detach().unsqueeze(-2)
    if tau.size(-1) != 1:
        tau = tau.detach().unsqueeze(-1)
    weight = weight.detach().unsqueeze(-2)
    expanded_inputs, expanded_targets = torch.broadcast_tensors(inputs, targets)

    L = F.smooth_l1_loss(expanded_inputs, expanded_targets, reduction="none")
    sign = torch.sign(expanded_inputs - expanded_targets) / 2. + 0.5
    rho = torch.abs(tau - sign) * L * weight
    return rho.sum(dim=-1).mean()


def module_hash(module):
    result = 0
    for tensor in module.state_dict().values():
        result += tensor.sum().item()
    return result


def make_dir(dir_path):
    try:
        os.mkdir(dir_path)
    except OSError:
        pass
    return dir_path


def preprocess_obs(obs, bits=5):
    """Preprocessing image, see https://arxiv.org/abs/1807.03039."""
    bins = 2**bits
    assert obs.dtype == torch.float32
    if bits < 8:
        obs = torch.floor(obs / 2**(8 - bits))
    obs = obs / bins
    obs = obs + torch.rand_like(obs) / bins
    obs = obs - 0.5
    return obs


class FrameStack(gym.Wrapper):
    def __init__(self, env, k):
        gym.Wrapper.__init__(self, env)
        self._k = k
        self._frames = deque([], maxlen=k)
        shp = env.observation_space.shape
        self.observation_space = gym.spaces.Box(
            low=0,
            high=1,
            shape=((shp[0] * k,) + shp[1:]),
            dtype=env.observation_space.dtype
        )
        self._max_episode_steps = env._max_episode_steps

    def reset(self):
        obs = self.env.reset()
        for _ in range(self._k):
            self._frames.append(obs)
        return self._get_obs()

    def step(self, action):
        obs, reward, done, info = self.env.step(action)
        self._frames.append(obs)
        return self._get_obs(), reward, done, info

    def _get_obs(self):
        assert len(self._frames) == self._k
        return np.concatenate(list(self._frames), axis=0)


# https://github.com/MishaLaskin/rad/blob/master/data_augs.py
def random_crop(imgs, out=84):
    """
        args:
        imgs: np.array shape (B,C,H,W)
        out: output size (e.g. 84)
        returns np.array
    """
    n, c, h, w = imgs.shape
    crop_max = h - out + 1
    w1 = np.random.randint(0, crop_max, n)
    h1 = np.random.randint(0, crop_max, n)
    cropped = np.empty((n, c, out, out), dtype=imgs.dtype)
    for i, (img, w11, h11) in enumerate(zip(imgs, w1, h1)):
        
        cropped[i] = img[:, h11:h11 + out, w11:w11 + out]
    return cropped


# https://github.com/MishaLaskin/rad/blob/master/utils.py
def center_crop_image(image, output_size):
    h, w = image.shape[1:]
    new_h, new_w = output_size, output_size

    top = (h - new_h)//2
    left = (w - new_w)//2

    image = image[:, top:top + new_h, left:left + new_w]
    return image


class eval_mode(object):
    def __init__(self, *models):
        self.models = models

    def __enter__(self):
        self.prev_states = []
        for model in self.models:
            self.prev_states.append(model.training)
            model.train(False)

    def __exit__(self, *args):
        for model, state in zip(self.models, self.prev_states):
            model.train(state)
        return False


class train_mode(object):
    def __init__(self, *models):
        self.models = models

    def __enter__(self):
        self.prev_states = []
        for model in self.models:
            self.prev_states.append(model.training)
            model.train(True)

    def __exit__(self, *args):
        for model, state in zip(self.models, self.prev_states):
            model.train(state)
        return False
