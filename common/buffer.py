import torch
from torch.utils.data import Dataset, DataLoader
import os
import numpy as np

from .utils import random_crop


class ReplayBuffer(Dataset):
    """Buffer to store environment transitions."""
    def __init__(self, obs_shape, action_shape, capacity, batch_size,
                 device, image_size=64):
        self.capacity = capacity
        self.batch_size = batch_size
        self.device = device
        self.image_size = image_size
        # the proprioceptive obs is stored as float32, pixels obs as uint8
        obs_dtype = np.float32 if len(obs_shape) == 1 else np.uint8
        
        self.obses = np.empty((capacity, *obs_shape), dtype=obs_dtype)
        self.next_obses = np.empty((capacity, *obs_shape), dtype=obs_dtype)
        self.actions = np.empty((capacity, *action_shape), dtype=np.float32)
        self.rewards = np.empty((capacity, 1), dtype=np.float32)
        self.not_dones = np.empty((capacity, 1), dtype=np.float32)

        self.episode_step = np.zeros(capacity,)

        self.idx = 0
        self.last_save = 0
        self.full = False
    
    @property
    def size(self):
        return self.capacity if self.full else self.idx
    
    def add(self, obs, action, reward, next_obs, done, ep_len):

        np.copyto(self.obses[self.idx], obs)
        np.copyto(self.actions[self.idx], action)
        np.copyto(self.rewards[self.idx], reward)
        np.copyto(self.next_obses[self.idx], next_obs)
        np.copyto(self.not_dones[self.idx], not done)
        self.episode_step[self.idx] = ep_len

        self.idx = (self.idx + 1) % self.capacity
        self.full = self.full or self.idx == 0

    def sample_idxs(self, batch_size):
        idxs = np.random.randint(0, self.size, size=batch_size)
        return idxs

    def sample(self, batch_size=None, crop=False):
        if batch_size is None:
            batch_size = self.batch_size

        idxs = self.sample_idxs(batch_size)
        
        obses = self.obses[idxs]
        next_obses = self.next_obses[idxs]

        if crop:
            obses = random_crop(obses, self.image_size)
            next_obses = random_crop(next_obses, self.image_size)

        obses = torch.as_tensor(obses, device=self.device).float()
        actions = torch.as_tensor(self.actions[idxs], device=self.device)
        rewards = torch.as_tensor(self.rewards[idxs], device=self.device)
        next_obses = torch.as_tensor(next_obses, device=self.device).float()
        not_dones = torch.as_tensor(self.not_dones[idxs], device=self.device)

        transition = dict(
            obs=obses, obs2=next_obses, act=actions, rew=rewards, not_done=not_dones
        )
        return transition
    
    # def sample_tcl(self, idxs, obses, pm):
    #     fs = obses.shape[1]//3  # frame_stack: fs
    #     assert pm <= fs

    #     obs = obses.reshape(self.batch_size, fs, 3, -1, obses.shape[-1])
    #     pos = self.next_obses[idxs]

    #     if pm == fs:
    #         t_pos = pos
    #     else:
    #         pos = pos.reshape(self.batch_size, fs, 3, -1, obses.shape[-1])
    #         t_pos = np.zeros(obs.shape)
    #         t_pos[:, :fs-pm], t_pos[:, fs-pm:] = obs[:, pm:], pos[:, :pm]
    #         t_pos = t_pos.reshape(obses.shape)
        
    #     if t_pos.shape[-1] != self.image_size:
    #         t_pos = center_crop(t_pos, self.image_size)
    #     t_pos = torch.as_tensor(t_pos, device=self.device).float()
        
    #     return t_pos

    def save(self, save_dir):
        if self.idx == self.last_save:
            return
        path = os.path.join(save_dir, '%d_%d.pt' % (self.last_save, self.idx))
        payload = [
            self.obses[self.last_save:self.idx],
            self.next_obses[self.last_save:self.idx],
            self.actions[self.last_save:self.idx],
            self.rewards[self.last_save:self.idx],
            self.not_dones[self.last_save:self.idx]
        ]
        self.last_save = self.idx
        torch.save(payload, path)

    def load(self, save_dir):
        chunks = os.listdir(save_dir)
        chucks = sorted(chunks, key=lambda x: int(x.split('_')[0]))
        for chunk in chucks:
            start, end = [int(x) for x in chunk.split('.')[0].split('_')]
            path = os.path.join(save_dir, chunk)
            payload = torch.load(path)
            assert self.idx == start
            self.obses[start:end] = payload[0]
            self.next_obses[start:end] = payload[1]
            self.actions[start:end] = payload[2]
            self.rewards[start:end] = payload[3]
            self.not_dones[start:end] = payload[4]
            self.idx = end

    def __getitem__(self, idx):
        idx = np.random.randint(0, self.size, size=1)
        idx = idx[0]
        obs = self.obses[idx]
        action = self.actions[idx]
        reward = self.rewards[idx]
        next_obs = self.next_obses[idx]
        not_done = self.not_dones[idx]

        if self.transform:
            obs = self.transform(obs)
            next_obs = self.transform(next_obs)

        return obs, action, reward, next_obs, not_done

    def __len__(self):
        return self.capacity

