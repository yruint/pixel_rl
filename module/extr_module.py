import torch
import torch.nn as nn


def tie_weights(src, trg):
    assert type(src) == type(trg)
    trg.weight = src.weight
    trg.bias = src.bias


# for 84 x 84 inputs
OUT_DIM = {2: 39, 4: 35, 6: 31}
# for 64 x 64 inputs
OUT_DIM_64 = {2: 29, 4: 25, 6: 21}
OUT_DIM_108 = {4: 47}


class PixelExtractor(nn.Module):
    """Convolutional encoder of pixels observations."""
    def __init__(self, obs_shape, feature_dim, num_layers=2, num_filters=32, ln=False):
        super().__init__()

        assert len(obs_shape) == 3
        self.obs_shape = obs_shape
        self.feature_dim = feature_dim
        self.num_layers = num_layers

        self.convs = nn.ModuleList(
            [nn.Conv2d(obs_shape[0], num_filters, 3, stride=2)]
        )
        for i in range(num_layers - 1):
            self.convs.append(nn.Conv2d(num_filters, num_filters, 3, stride=1))
            # self.convs.append(nn.BatchNorm2d(num_filters))

        if obs_shape[-1] == 64:
            out_dim = OUT_DIM_64[num_layers]
        elif obs_shape[-1] == 108:
            assert num_layers in OUT_DIM_108
            out_dim = OUT_DIM_108[num_layers]
        else:
            out_dim = OUT_DIM[num_layers]

        self.fc = nn.Linear(num_filters * out_dim * out_dim, self.feature_dim)
        self.ln = nn.LayerNorm(self.feature_dim) if ln else nn.Identity()
        # self.ln = nn.Identity()

    def forward_conv(self, obs):
        obs = obs / 255.
        conv = torch.relu(self.convs[0](obs))
        for i in range(1, self.num_layers):
            conv = torch.relu(self.convs[i](conv))
        return conv

    def forward(self, obs, conv_detach=False, detach=False):
        h = self.forward_conv(obs).view(obs.size(0), -1)
        if conv_detach:
            h = h.detach()

        h_fc = self.fc(h)
        z = self.ln(h_fc)
        if detach:
            z = z.detach()
        return z

    def forward_no_ln(self, obs):
        h = self.forward_conv(obs).view(obs.size(0), -1)
        return self.fc(h)

    def out_conv(self, obs):
        h = self.forward_conv(obs).view(obs.size(0), -1)
        return h

    # def copy_conv_weights_from(self, source):
    #     """Tie convolutional layers and hidden layers"""
    #     tie_weights(src=source.fc, trg=self.fc)
    #     for i in range(self.num_layers):
    #         tie_weights(src=source.convs[i], trg=self.convs[i])


class IdentityEncoder(nn.Module):
    def __init__(self, obs_shape, feature_dim, num_layers, num_filters, *args):
        super().__init__()

        assert len(obs_shape) == 1
        self.feature_dim = obs_shape[0]

    def forward(self, obs, detach=False):
        return obs

    def copy_conv_weights_from(self, source):
        pass


_AVAILABLE_ENCODERS = {'pixel': PixelExtractor, 'identity': IdentityEncoder}


def make_extr(
    encoder_type, obs_shape, extr_latent_dim, num_layers, num_filters, ln, **args
):
    assert encoder_type in _AVAILABLE_ENCODERS
    return _AVAILABLE_ENCODERS[encoder_type](
        obs_shape, extr_latent_dim, num_layers, num_filters, ln
    )