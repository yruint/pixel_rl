import numpy as np
import torch
import torch.nn as nn

import common.utils as utils
from module.extr_module import make_extr
from module.rl_module import CURL


def init_extr_learning_method(extr, obs_shape, device, config):
    out_dic = None
    if config['encoder_type'] == 'pixel':
        if config['sparse']:
            extr_lr, extr_tau = config['extr_lr'], config['extr_tau']
            extr_update_freq = config['extr_update_freq']
            extr_optimizer = torch.optim.Adam(extr.parameters(), lr=extr_lr)#, betas=(extr_beta, 0.999))
            out_dic = dict(
                extr_update_freq=extr_update_freq, extr_tau=extr_tau,
                extr_optimizer=extr_optimizer
            )
        if config['cpc']:
            cpc_lr, cpc_tau = config['cpc_lr'], config['cpc_tau']
            cpc_update_freq = config['cpc_update_freq']

            extr_targ = make_extr(obs_shape=obs_shape, **config).to(device)
            extr_targ.convs.load_state_dict(extr.convs.state_dict())
            extr_targ.fc.load_state_dict(extr.fc.state_dict())
            
            curl = CURL(extr, extr_targ, config['extr_latent_dim']).to(device)
            curl_optimizer = torch.optim.Adam(curl.parameters(), lr=cpc_lr)#, betas=(cpc_beta, 0.999))
            out_dic = dict(
                cpc_update_freq=cpc_update_freq, cpc_tau=cpc_tau,
                extr_targ=extr_targ, curl=curl, curl_optimizer=curl_optimizer
            )
    return out_dic


from algo import *

_AVAILABLE_ALGORITHM = {'sac': SACAgent}

def init_agent(alg, extr_config, config):
    assert alg in _AVAILABLE_ALGORITHM
    return _AVAILABLE_ALGORITHM[alg](extr_config=extr_config, **config)
