import numpy as np
import math
import torch
import torch.nn as nn
import torch.nn.functional as F


def gaussian_logprob(noise, log_std):
    """Compute Gaussian log probability."""
    residual = (-0.5 * noise.pow(2) - log_std).sum(-1, keepdim=True)
    return residual - 0.5 * np.log(2 * np.pi) * noise.size(-1)


def squash(mu, pi, log_pi):
    """Apply squashing function.
    See appendix C from https://arxiv.org/pdf/1812.05905.pdf.
    """
    mu = torch.tanh(mu)
    if pi is not None:
        pi = torch.tanh(pi)
    if log_pi is not None:
        log_pi -= torch.log(F.relu(1 - pi.pow(2)) + 1e-6).sum(-1, keepdim=True)
        log_pi = log_pi.squeeze(-1)
    return mu, pi, log_pi


class EnsembleLinear(nn.Module):

    def __init__(self, in_features, out_features, in_channels, bias=True):
        super(EnsembleLinear, self).__init__()
        
        self.in_channels = in_channels
        self.in_features = in_features
        self.out_features = out_features
        self.weight = nn.Parameter(torch.empty((in_channels, in_features, out_features)))
        if bias:
            self.bias = nn.Parameter(torch.empty(in_channels, 1, out_features))
        else:
            self.register_parameter('bias', None)
        self.reset_parameters()

    def reset_parameters(self):
        # Setting a=sqrt(5) in kaiming_uniform is the same as initializing with
        # uniform(-1/sqrt(in_features), 1/sqrt(in_features)). For details, see
        # https://github.com/pytorch/pytorch/issues/57109
        nn.init.kaiming_uniform_(self.weight, a=math.sqrt(5))
        if self.bias is not None:
            fan_in, _ = nn.init._calculate_fan_in_and_fan_out(self.weight)
            bound = 1 / math.sqrt(fan_in) if fan_in > 0 else 0
            nn.init.uniform_(self.bias, -bound, bound)

    def forward(self, input):
        # input: (in_channels, batch_size, in_features)
        output = input.matmul(self.weight) + self.bias if self.bias is not None else input.matmul(self.weight)
        return output # output: (batch_size, in_channels, out_features)

    def extra_repr(self):
        return 'in_features={}, out_features={}, in_channels={}, bias={}'.format(
            self.in_features, self.out_features, self.in_channels, self.bias is not None
        )


def weight_init(m):
    """Custom weight init for Conv2D and Linear layers."""
    if isinstance(m, EnsembleLinear) or isinstance(m, nn.Linear):
        nn.init.orthogonal_(m.weight.data)
        m.bias.data.fill_(0.0)
    elif isinstance(m, nn.Conv2d) or isinstance(m, nn.ConvTranspose2d):
        # delta-orthogonal init from https://arxiv.org/pdf/1806.05393.pdf
        assert m.weight.size(2) == m.weight.size(3)
        m.weight.data.fill_(0.0)
        m.bias.data.fill_(0.0)
        mid = m.weight.size(2) // 2
        gain = nn.init.calculate_gain('relu')
        nn.init.orthogonal_(m.weight.data[:, :, mid, mid], gain)


def mlp(input_dim=None, hidden_dim=1024, output_dim=None, hidden_depth=2,
        output_mod=None, inplace=False, handle_dim=None, channel_dim=1, linear=nn.Linear):

    if isinstance(linear, nn.Linear):
        linear = lambda n_input, n_output, channel_dim: nn.Linear(n_input, n_output)
    elif linear == 'spectral_norm':
        linear = lambda n_input, n_output, channel_dim: nn.utils.spectral_norm(nn.Linear(n_input, n_output))
    elif isinstance(linear, EnsembleLinear):
        linear = lambda n_input, n_output, channel_dim: EnsembleLinear(
            in_features=n_input, out_features=n_output, in_channels=channel_dim
        )

    if hidden_depth == 0:
        mods = [linear(input_dim, output_dim, channel_dim)]
    else:
        mods = [linear(input_dim, hidden_dim, channel_dim), nn.ReLU(inplace=True)]
        for i in range(hidden_depth - 1):
            mods += [linear(hidden_dim, hidden_dim, channel_dim), nn.ReLU(inplace=True)]
        mods.append(linear(hidden_dim, output_dim, channel_dim))
    if output_mod is not None:
        try:
            mods.append(output_mod(inplace=inplace))
        except:
            if handle_dim is not None:
                mods.append(output_mod(dim=handle_dim))
            else:
                mods.append(output_mod())
    trunk = nn.Sequential(*mods)
    return trunk


class SGMLPActor(nn.Module):
    
    def __init__(
        self, action_shape, hidden_dim, encoder_feature_dim,
        log_std_min, log_std_max, l, action_limit
    ):
        super(SGMLPActor, self).__init__()

        self.log_std_min = log_std_min
        self.log_std_max = log_std_max
        self.act_limit = action_limit
        self.state_dim = encoder_feature_dim

        self.pi = mlp(self.state_dim, hidden_dim, 2 * action_shape[0], l)
        self.apply(weight_init)

    def output(self, pi):
        if pi is None:
            return None
        return self.act_limit * pi

    def forward(
        self, state, compute_pi=True, with_logprob=True
    ):
        mu, log_std = self.pi(state).chunk(2, dim=-1)

        # constrain log_std inside [log_std_min, log_std_max]
        log_std = torch.tanh(log_std)
        log_std = self.log_std_min + 0.5 * (
            self.log_std_max - self.log_std_min
        ) * (log_std + 1)

        if compute_pi:
            std = log_std.exp()
            noise = torch.randn_like(mu)
            pi = mu + noise * std
        else:
            pi = None

        if with_logprob:
            log_pi = gaussian_logprob(noise, log_std)
        else:
            log_pi = None

        mu, pi, log_pi = squash(mu, pi, log_pi)

        return self.output(mu), self.output(pi), log_pi, log_std
    
    def act(self, state, deterministic=False):
        mu_action, pi_action, _, _ = self.forward(state, with_logprob=False)
        if deterministic:
            return mu_action
        return pi_action


class MLPActor(nn.Module):

    def __init__(self, action_shape, hidden_dim, encoder_feature_dim, l, act_limit, act_noise=0.1):
        super(MLPActor, self).__init__()
        self.act_limit = act_limit
        self.act_noise = act_noise
        self.state_dim = encoder_feature_dim

        self.pi = mlp(self.state_dim, hidden_dim, action_shape[0], l, nn.Tanh)
        self.apply(weight_init)

    def forward(
        self, state, deterministic=False, act_noise=None, clip=False, noise_clip=0.5
    ):
        mu = self.act_limit * self.pi(state)

        if deterministic:
            pi_action = mu
        else:
            if act_noise is None: act_noise = self.act_noise
            noise = torch.randn_like(mu) * act_noise
            if clip:
                noise = noise.clamp(-noise_clip, noise_clip)
            pi_action = (mu + noise).clamp(-self.act_limit, self.act_limit)

        return pi_action

    def act(self, state, deterministic=False):
        action = self.forward(state, deterministic)
        return action


class Critic(nn.Module):
    """Critic network, employes two q-functions."""
    def __init__(self, action_shape, hidden_dim, encoder_feature_dim, l=2,
                 output_mod=None, num_q=2, output_dim=1):
        super(Critic, self).__init__()

        self.state_dim = encoder_feature_dim
        self.output_dim = output_dim
        
        self.q1 = mlp(self.state_dim + action_shape[0], hidden_dim, output_dim, l, output_mod)
        if num_q == 1:
            self.q2 = None
        else:
            self.q2 = mlp(self.state_dim + action_shape[0], hidden_dim, output_dim, l, output_mod)
        self.apply(weight_init)

    def forward(self, state, action):
        assert state.size(0) == action.size(0)
        sa = torch.cat([state, action], 1)
        q1 = self.q1(sa)
        q2 = self.q2(sa)
        # return q1, q2
        return torch.squeeze(q1, -1), torch.squeeze(q2, -1) # q:(batch_size,)

    def Q1(self, state, action):
        assert state.size(0) == action.size(0)
        sa = torch.cat([state, action], 1)
        return torch.squeeze(self.q1(sa), -1)


class EnsembleCritic(nn.Module):

    def __init__(self, action_shape, hidden_dim, encoder_feature_dim, l=2,
                 output_mod=None, num_q=2, output_dim=1, handle_dim=None):
        super(EnsembleCritic, self).__init__()

        self.state_dim = encoder_feature_dim
        self.output_dim = output_dim
        self.num_q = num_q

        self.q = mlp(
            self.state_dim + action_shape[0], hidden_dim, output_dim, l,
            output_mod, handle_dim=handle_dim, channel_dim=num_q, linear=EnsembleLinear
        )
        self.apply(weight_init)

    def forward(self, state, action, minimize=True):
        assert state.size(0) == action.size(0)
        sa = torch.cat([state, action], -1)
        q = self.q(sa) # (batch_size, 1) or (num_q, batch_size, 1)
        q = torch.squeeze(q, -1) if q.size(-1) == 1 else q # q:(num_q, batch_size)
        if minimize:
            q = q.min(dim=0)[0] if q.size(0) == self.num_q else q # q:(batch_size,)
        return q

    def Q1(self, state, action=None):
        if action is None:
            sa = state
        else:
            assert state.size(0) == action.size(0)
            sa = torch.cat([state, action], -1)
        q = self.q(sa)
        q = torch.squeeze(q, -1) if q.size(-1) == 1 else q
        q = q[0] if q.size(0) == self.num_q else q
        return q


class QuantCritic(nn.Module):

    def __init__(self, action_shape, cosines_dim, hidden_dim, encoder_feature_dim,
                 l=2, output_mod=None, num_q=2, output_dim=1):
        super(QuantCritic, self).__init__()
        self.state_dim = encoder_feature_dim

        self.q1_sa = mlp(self.state_dim+action_shape[0], 0, hidden_dim, 0, nn.ReLU, True)
        self.q1_tau = mlp(cosines_dim, 0, hidden_dim, 0, nn.ReLU, True)
        self.q1 = mlp(hidden_dim, hidden_dim, output_dim, l-1, output_mod)
        self.q2_sa, self.q2_tau, self.q2 = None, None, None
        if num_q == 2:
            self.q2_sa = mlp(self.state_dim+action_shape[0], 0, hidden_dim, 0, nn.ReLU, True)
            self.q2_tau = mlp(cosines_dim, 0, hidden_dim, 0, nn.ReLU, True)
            self.q2 = mlp(hidden_dim, hidden_dim, output_dim, l-1, output_mod)
        self.range_pi = torch.arange(start=1, end=cosines_dim+1, dtype=torch.float32) * np.pi
        self.apply(weight_init)

    def forward(self, state, action, tau):
        """
        state:  (batch_size, obs_dim)
        action: (batch_size, act_dim)
        tau:    (batch_size, num_quantiles)
        """
        assert state.size(0) == action.size(0) == tau.size(0)
        sa = torch.cat([state, action], -1)
        q1 = self.q1_sa(sa) # (batch_size, hidden_dim)
        q2 = self.q2_sa(sa) # (batch_size, hidden_dim)

        if self.range_pi.device != tau.device:
            self.range_pi = self.range_pi.to(tau.device)

        cosines = torch.cos(tau.unsqueeze(-1) * self.range_pi) # (batch_size, num_quantiles, cosines_dim)

        tau1 = self.q1_tau(cosines) # (batch_size, num_quantiles, hidden_dim)
        tau2 = self.q2_tau(cosines) # (batch_size, num_quantiles, hidden_dim)

        q1 = torch.mul(tau1, q1.unsqueeze(-2)) # (batch_size, num_quantiles, hidden_dim)
        q2 = torch.mul(tau2, q2.unsqueeze(-2)) # (batch_size, num_quantiles, hidden_dim)
        # q: (batch_size, num_quantiles)
        return self.q1(q1).squeeze(-1), self.q2(q2).squeeze(-1)

    def Q1(self, state, action, tau):
        assert state.size(0) == action.size(0) == tau.size(0)
        sa = torch.cat([state, action], -1)
        q = self.q1_sa(sa)

        if self.range_pi.device != tau.device:
            self.range_pi = self.range_pi.to(tau.device)
        
        cosines = torch.cos(tau.unsqueeze(-1) * self.range_pi)
        tau = self.q1_tau(cosines)
        q = torch.mul(tau, q.unsqueeze(-2))
        return torch.squeeze(self.q1(q), -1)


class EnsembleQuantCritic(nn.Module):

    def __init__(self, action_shape, cosines_dim, hidden_dim, encoder_feature_dim,
                 l=2, output_mod=None, num_q=2, output_dim=1):
        super(EnsembleQuantCritic, self).__init__()
        self.state_dim = encoder_feature_dim
        self.num_q = num_q

        self.q_sa = mlp(self.state_dim+action_shape[0], 0, hidden_dim, 0, nn.ReLU, True, None, num_q, EnsembleLinear)
        self.q_tau = mlp(cosines_dim, 0, hidden_dim, 0, nn.ReLU, True, None, num_q, EnsembleLinear)
        self.q_net = mlp(hidden_dim, hidden_dim, output_dim, l-1, output_mod, channel_dim=num_q, linear=EnsembleLinear)

        self.range_pi = torch.arange(start=1, end=cosines_dim+1, dtype=torch.float32) * np.pi
        self.apply(weight_init)

    def forward(self, state, action, tau):
        """
        state:  (batch_size, obs_dim)
        action: (batch_size, act_dim)
        tau:    (batch_size, num_quantiles)
        """
        assert state.size(0) == action.size(0) == tau.size(0)
        batch_size = state.size(0)
        sa = torch.cat([state, action], -1)
        q = self.q_sa(sa) # (num_q, batch_size, hidden_dim)

        if self.range_pi.device != tau.device:
            self.range_pi = self.range_pi.to(tau.device)

        cosines = torch.cos(tau.unsqueeze(-1) * self.range_pi) # (batch_size, num_quantiles, cosines_dim)
        cosines = cosines.unsqueeze(1).expand(batch_size, self.num_q, cosines.size(1), -1) # (batch_size, num_q, num_quantiles, cosines_dim)

        tau = self.q_tau(cosines).transpose(1, 0) # (num_q, batch_size, num_quantiles, hidden_dim)

        q = torch.mul(tau, q.unsqueeze(-2)).view(tau.size(0), -1, tau.size(-1))
        # (num_q, batch_size*num_quantiles, hidden_dim)

        q = self.q_net(q).squeeze(-1).view(*tau.size()[:-1])
        return q # (num_q, batch_size, num_quantiles)

    def Q1(self, state, action, tau):
        q = self.forward(state, action, tau)
        return q[0]


class CURL(nn.Module):

    def __init__(self, extr, extr_target, feature_dim):
        super(CURL, self).__init__()
        self.extr = extr
        self.extr_target = extr_target
        self.W = nn.Parameter(torch.rand(feature_dim, feature_dim))
    
    def encode(self, x, detach=False, ema=False):
        if ema:
            z_out = self.extr_target(x).detach()
        else:
            z_out = self.extr(x)
        if detach:
            z_out = z_out.detach()
        return z_out

    def compute_logits(self, z_anc, z_pos):
        Wz = torch.matmul(self.W, z_pos.T)
        logits = torch.matmul(z_anc, Wz)
        logits = logits - torch.max(logits, 1)[0][:, None]
        return logits
