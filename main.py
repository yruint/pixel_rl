import torch
import json
import copy
import random
import numpy as np
# import gym, dmc2gym
import os, time, argparse

from common import utils, buffer
from common.logger_tb import Logger
from common.logx import EpochLogger, setup_logger_kwargs
# from common import logger
from common.video import VideoRecorder

from argument import parse_args
from module.make_agent import init_agent
from train import train_agent


def main(args, device, config, extr_config):
    if args.seed == -1: 
        args.__dict__["seed"] = np.random.randint(1,1000000)
    utils.set_seed_everywhere(args.seed)
    _, domain_name, task_name = args.env.split('.')

    # make directory
    cpc = '-cpc' if args.cpc else ''
    crop = '-crop' if args.crop else ''
    sparse = f'-sparse-smoothl1-{args.extr_lr}' if args.sparse else ''
    ln = "-ln" if args.ln else ''
    mode = f"-fs{args.frame_stack}-ar{args.action_repeat}{cpc}{crop}{sparse}{ln}"
    exp_name = f"{args.env}-im{args.image_size}-b{args.batch_size}" + mode
    config['exp_name'] = exp_name

    work_dir = os.path.join(os.path.abspath(os.path.dirname(
        os.path.dirname(__file__))), 'data', f'{args.env}'
    )
    logger_kwargs = setup_logger_kwargs(exp_name, args.seed, work_dir)

    # Setting logger and save hyperparameters
    L = EpochLogger(**logger_kwargs)
    L.save_config(locals())

    work_dir = os.path.join(work_dir, f'{exp_name}', f'{exp_name}_s{args.seed}')

    # utils.make_dir(work_dir)
    TBL = Logger(work_dir, use_tb=args.save_tb)
    video_dir = utils.make_dir(os.path.join(work_dir, 'video'))
    model_dir = utils.make_dir(os.path.join(work_dir, 'model'))
    buffer_dir = utils.make_dir(os.path.join(work_dir, 'buffer'))

    video = VideoRecorder(video_dir if args.save_video else None)

    # Setting Environment
    pre_image_size = args.pre_image_size if args.crop else args.image_size
    env = utils.make_env(domain_name, task_name, args.seed, args.video_alpha, args)
    env.seed(args.seed)
    # stack several consecutive frames together
    if args.encoder_type == 'pixel':
        env = utils.FrameStack(env, k=args.frame_stack)
        obs_shape = (3*args.frame_stack, args.image_size, args.image_size)
        pre_aug_obs_shape = (3*args.frame_stack, pre_image_size, pre_image_size)
    else:
        obs_shape = env.observation_space.shape
        pre_aug_obs_shape = obs_shape

    # Setting Test Environment
    if args.test:
        # test_seed = np.random.randint(0, 1000000) + args.seed
        test_seed = 1000 + args.seed
        test_env = []
        t_env = utils.make_env(domain_name, task_name, test_seed, args.video_alpha, args)
        if args.encoder_type == 'pixel':
            t_env = utils.FrameStack(t_env, k=args.frame_stack)
            t_env.seed(test_seed + 1)
        test_env.append(t_env)

        if args.num_test_env == 1:
            t_env = utils.make_env(domain_name, task_name, test_seed + 2, args.test_video_alpha, args)
            if args.encoder_type == 'pixel':
                t_env = utils.FrameStack(t_env, k=args.frame_stack)
                t_env.seed(test_seed + 2)
            test_env.append(t_env)
        else:
            for i in range(1, 11):
                t_env = utils.make_env(domain_name, task_name, test_seed + i + 1, i / 10, args)
                if args.encoder_type == 'pixel':
                    t_env = utils.FrameStack(t_env, k=args.frame_stack)
                t_env.seed(test_seed + i + 1)
                test_env.append(t_env)
    else:
        test_env = None

    torch.manual_seed(args.seed)
    if args.cuda:
        torch.cuda.manual_seed(args.seed)
    random.seed(args.seed)
    np.random.seed(args.seed)

    action_shape = env.action_space.shape
    action_limit = env.action_space.high[0]

    replay_buffer = buffer.ReplayBuffer(
        obs_shape=pre_aug_obs_shape,
        action_shape=action_shape,
        capacity=args.replay_buffer_capacity,
        batch_size=args.batch_size,
        device=device,
        image_size=args.image_size,
    )
    print(replay_buffer.obses.shape)

    kwargs = {
        "obs_shape": obs_shape,
        "action_shape": action_shape,
        "action_limit": action_limit,
        "device": device
    }
    config.update(kwargs)

    agent = init_agent(args.alg, extr_config, config)
    print(agent.extr)
    print(agent.actor)
    print(agent.critic)

    train_agent(
        env=env,
        test_env=test_env,
        agent=agent,
        replay_buffer=replay_buffer,
        L=L, TBL=TBL,
        video=video,
        model_dir=model_dir,
        buffer_dir=buffer_dir,
        init_steps=config['init_steps'],
        device=device,
        work_dir=work_dir,
        args=args
    )
    
    env.close()
    if isinstance(test_env, list):
        for t_env in test_env:
            t_env.close()
    else:
        test_env.close()


if __name__ == '__main__':

    args = parse_args()
    cuda_id = "cuda:" + str(args.cuda_id)
    device = torch.device(cuda_id if args.cuda else "cpu")
    config = utils.read_config(args, args.config_dir)
    # variant = copy.deepcopy(config)
    extr_config = utils.set_extr_config(device, config)
    for i in args.seed_list:
        torch.multiprocessing.set_start_method('spawn', force=True)
        args.seed, config['seed'] = i, i
        main(args, device, config, extr_config)
