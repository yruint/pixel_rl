Apply another method to learn a new feature extractor:
    1. module.extr_module.make_extr:
        initialize feature extractor
    2. common.utils.set_extr_config:
        add new hyperparameters
    3. algo.agent._init_extractor:
        module.make_agent.init_extr_learning_method:
            initialize the new module
    4. algo.agent:
        add the new def to update new module

Add another rl algorithm to learn an agent:
    add to algo/
    follow algo/sac.py