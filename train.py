import numpy as np
import torch
import time

import common.utils as utils
# from common import logger


def evaluate(num, env, agent, L, TBL, video, num_episodes, step, start_time, work_dir, args):
    all_ep_rewards = []
    all_ep_length = []

    def run_eval_loop(num_episodes, deterministic=False):
        start_time = time.time()
        for _ in range(num_episodes):
            obs = env.reset()
            # video.init(enabled=(i == 0))
            done = False
            episode_reward, episode_length = 0, 0
            while not done:
                with utils.eval_mode(agent):
                    action = agent.select_action(obs, deterministic=deterministic)
                obs, reward, done, _ = env.step(action)
                # video.record(env)
                episode_reward += reward
                episode_length += 1

            # video.save('%d.mp4' % step)
            if num is None:
                test_info = {"TestEpRet": episode_reward, "TestEpLen": episode_length}
            else:
                test_info = {f"TestEpRet{num}": episode_reward, f"TestEpLen{num}": episode_length}
            L.store(**test_info)
            TBL.log('eval/episode_reward', episode_reward, step)
            all_ep_rewards.append(episode_reward)
            all_ep_length.append(episode_length)

        mean_ep_reward = np.mean(all_ep_rewards)
        best_ep_reward = np.max(all_ep_rewards)
        std_ep_reward = np.std(all_ep_rewards)
        TBL.log('eval/mean_episode_reward', mean_ep_reward, step)
        TBL.log('eval/best_episode_reward', best_ep_reward, step)
        TBL.log('eval/std_episode_reward', std_ep_reward, step)
        TBL.log('eval/eval_time', (time.time() - start_time)/3600, step)
    
        filename = work_dir + '/' + args.env + '-s' + str(args.seed) + '--eval_scores.npy'
        key = args.env
        try:
            log_data = np.load(filename,allow_pickle=True)
            log_data = log_data.item()
        except:
            log_data = {}
            
        if key not in log_data:
            log_data[key] = {}

        log_data[key][step] = {}
        log_data[key][step]['step'] = step 
        log_data[key][step]['mean_ep_reward'] = mean_ep_reward 
        log_data[key][step]['max_ep_reward'] = best_ep_reward 
        log_data[key][step]['std_ep_reward'] = std_ep_reward 
        log_data[key][step]['env_step'] = step * args.action_repeat

        np.save(filename, log_data)
    
    run_eval_loop(num_episodes, True)
    # TBL.dump(step)


def train_agent(env, test_env, agent, replay_buffer, L, TBL, video, model_dir, buffer_dir,
                init_steps, device, work_dir, args):

    episode, episode_reward, episode_step, best_return, done = 0, 0, 0, 0, False
    start_time = time.time()
    o = env.reset()
    # import pdb
    # pdb.set_trace()
    for step in range(args.total_steps):

        # sample action for data collection
        if step < init_steps:
            a = env.action_space.sample()
        else:
            
            # import pdb
            # pdb.set_trace()
            with utils.eval_mode(agent):
                a = agent.select_action(o)

        # run training update
        if step >= init_steps and step % args.num_updates == 0:
            for _ in range(args.num_updates):
                agent.update(replay_buffer, L, TBL, step)

        o2, r, done, _ = env.step(a)
        agent.total_time_steps += 1

        episode_reward += r
        episode_step += 1
        # allow infinit bootstrap
        done_bool = 0 if episode_step == env._max_episode_steps else float(
            done
        )
        replay_buffer.add(o, a, r, o2, done_bool, episode_step)

        # evaluate agent periodically
        if step % args.eval_freq == 0 and step > init_steps:
            if args.test:
                TBL.log('eval/episode', episode, step)
                if isinstance(test_env, list):
                    for num, t_env in enumerate(test_env):
                        evaluate(
                            num, t_env, agent, L, TBL,
                            video, args.num_eval_episodes,
                            step, start_time, work_dir, args
                        )
                else:
                    evaluate(
                        None, test_env, agent, L, TBL,
                        video, args.num_eval_episodes,
                        step, start_time, work_dir, args
                    )

            print_log(L, test_env, (step+1)//args.eval_freq, step, args.test, start_time, args)
       
            if args.save_model or step == 100000 or step == 500000:
                agent.save(model_dir, step)
            if args.save_buffer:
                replay_buffer.save(buffer_dir)

        if done:
            L.store(EpRet=episode_reward, EpLen=episode_step, EpNum=episode)

            TBL.log('train/episode_reward', episode_reward, step)
            TBL.log('train/episode', episode, step)
            TBL.log('train/times', (time.time() - start_time)/3600, step)
            # TBL.dump(step)
            print("Total T: {} Reward: {:.3f} Episode Num: {} Episode T: {}".format(
                         step+1, episode_reward, episode, episode_step))
            if best_return < episode_reward:
                best_return = episode_reward
                agent.save(model_dir, 'best')
            o, done, episode_reward, episode_step = env.reset(), False, 0, 0
            episode += 1

        o = o2


def print_log(logger, test_env, epoch, step, test, start_time, args):
    # Log info about epoch
    logger.log_tabular('Epoch', epoch)
    logger.log_tabular('TotalEnvInteracts', step+1)
    logger.log_tabular('EpRet', with_min_and_max=True)
    logger.log_tabular('EpNum', average_only=True)
    logger.log_tabular('EpLen', average_only=True)
    if test:
        if isinstance(test_env, list):
            for i in range(len(test_env)):
                logger.log_tabular(f'TestEpRet{i}', with_min_and_max=True)
        else:
            logger.log_tabular('TestEpRet', with_min_and_max=True)
            logger.log_tabular('TestEpLen', average_only=True)
    logger.log_tabular('Qvals', average_only=True)
    logger.log_tabular('Qmaxs', average_only=True)
    logger.log_tabular('TQvals', average_only=True)
    logger.log_tabular('TQmaxs', average_only=True)
    logger.log_tabular('LossQ', average_only=True)
    logger.log_tabular('LossPi', average_only=True)
    if args.alg == 'sac':
        logger.log_tabular('HPi', average_only=True)
        logger.log_tabular('Entro', average_only=True)
        logger.log_tabular('Alpha', average_only=True)
        logger.log_tabular('LossAlpha', average_only=True)
    # if args.sparse:
    logger.log_tabular('LossExtrL1', average_only=True)
    logger.log_tabular('LossExtrSmoothL1', average_only=True)
    logger.log_tabular('SparseNum3', average_only=True)
    logger.log_tabular('SparseNum5', average_only=True)
    if args.cpc:
        logger.log_tabular('LossCURL', average_only=True)
    logger.log_tabular('Time', (time.time() - start_time)/3600)
    logger.dump_tabular()
