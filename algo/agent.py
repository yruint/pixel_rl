import abc
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F

import common.utils as utils
from module.extr_module import make_extr
from module.make_agent import init_extr_learning_method


class BaseAgent(object, metaclass=abc.ABCMeta):
    
    def __init__(
        self, obs_shape, actor_update_freq, critic_tau,
        critic_target_update_freq, update_to_data, device,
        action_limit, gamma, sparse, cpc, crop, extr_config
    ):
        # Setting hyperparameters
        self.actor_update_freq = actor_update_freq
        self.action_limit = action_limit
        self.critic_target_update_freq = critic_target_update_freq
        self.critic_tau = critic_tau
        self.gamma = gamma
        self.device = device
        self.image_size = obs_shape[-1]
        self.update_to_data = update_to_data
        self.sparse, self.cpc, self.crop = sparse, cpc, crop
        self.encoder_type = extr_config['encoder_type']
        self.training = False
        
        self.total_time_steps = 0
        self.update_steps = 0
        self.update_critic_steps = 0
        self.update_actor_steps = 0
        self.update_extr_steps = 0

        # Setting modules
        self.extr = make_extr(obs_shape=obs_shape, **extr_config).to(device)
        self.extr_targ = None
        self.curl = None
        self.actor = None
        self.actor_targ = None
        self.critic = None
        self.critic_targ = None
        
        # Setting optimizers
        self.actor_optimizer = None
        self.critic_optimizer = None
        self.extr_q_optimizer = None
        self.extr_optimizer = None
        self.curl_optimizer = None

        self._init_extractor(obs_shape, extr_config)

    def _init_extractor(self, obs_shape, extr_config):
        module_dic = init_extr_learning_method(self.extr, obs_shape, self.device, extr_config)
        if self.encoder_type == 'pixel':
            if self.sparse:
                self.extr_update_freq = module_dic['extr_update_freq']
                self.extr_tau = module_dic['extr_tau']
                self.extr_optimizer = module_dic['extr_optimizer']
            if self.cpc:
                self.cpc_update_freq = module_dic['cpc_update_freq']
                self.cpc_tau = module_dic['cpc_tau']
                self.extr_targ = module_dic['extr_targ']
                self.curl = module_dic['curl']
                self.curl_optimizer = module_dic['curl_optimizer']

    def train(self, training=True):
        self.training = training
        self.actor.train(training)
        self.critic.train(training)
        self.extr.train(self.training) if self.extr is not None else None
        self.curl.train(self.training) if self.curl is not None else None

    def train_targ(self):
        self.extr_targ.train(self.training) if self.extr_targ is not None else None
        self.actor_targ.train(self.training) if self.actor_targ is not None else None
        self.critic_targ.train(self.training) if self.critic_targ is not None else None

    def handle_obs(self, o, conv_d=False, d=False):
        return self.extr(o, conv_d, d)

    def select_action(self, o, deterministic=False):
        # center crop image
        if self.encoder_type == 'pixel' and o.shape[-1] != self.image_size:
            o = utils.center_crop_image(o, self.image_size)
        with torch.no_grad():
            o = torch.FloatTensor(o).to(self.device)
            o = o.unsqueeze(0)
            s = self.handle_obs(o)
            pi = self.actor.act(s, deterministic)
            return pi.cpu().data.numpy().flatten()

    @abc.abstractmethod
    def update_critic(self, s, a, r, s2, nd, L, TBL, step):
        pass

    @abc.abstractmethod
    def update_actor(self, s, L, TBL, step):
        pass

    def update_curl(self, o, L, TBL, step):
        z_a = self.curl.encode(o)
        z_p = self.curl.encode(o, ema=True)

        logits = self.curl.compute_logits(z_a, z_p)
        labels = torch.arange(logits.shape[0]).long().to(self.device)
        loss_curl = nn.CrossEntropyLoss()(logits, labels)

        utils.update_params(
            dict(optim1=self.extr_optimizer, optim2=self.curl_optimizer), loss_curl
        )

        L.store(LossCURL=loss_curl.item())
        TBL.log('train_curl/loss', loss_curl, step)

    def update_extr(self, o, L, TBL, step):
        self._update_extr_l1(o, L, TBL, step)

    def _update_extr_l1(self, o, L, TBL, step):
        state = self.extr.forward_no_ln(o)
        assert state.ndim == 2
        loss_extr_l1 = state.abs().sum(-1).mean()
        loss_extr_smooth_l1 = F.smooth_l1_loss(state, torch.zeros_like(state))
        sparse_num_3 = (state.abs() < 0.001).float().sum(dim=-1).mean().item()
        sparse_num_5 = (state.abs() < 0.00001).float().sum(dim=-1).mean().item()

        utils.update_params(self.extr_optimizer, loss_extr_smooth_l1)

        L.store(LossExtrL1=loss_extr_l1.item(),
                LossExtrSmoothL1=loss_extr_smooth_l1.item(),
                SparseNum3=sparse_num_3, SparseNum5=sparse_num_5)
        TBL.log('train_sparse/loss_l1', loss_extr_l1.item(), step)
        TBL.log('train_sparse/loss_smoothl1', loss_extr_smooth_l1.item(), step)
        TBL.log('train_sparse/sparse_num3', sparse_num_3, step)
        TBL.log('train_sparse/sparse_num5', sparse_num_5, step)

    def update(self, replay_buffer, L, TBL, step, batch_size=None):
        self.update_steps += 1
        # Sample a batch from replay buffer
        data = replay_buffer.sample(batch_size, self.crop)
        o, a, r, o2, nd = data['obs'], data['act'], data['rew'], data['obs2'], data['not_done']
        r = r.squeeze(-1) if r.ndim == 2 else r
        nd = nd.squeeze(-1) if nd.ndim == 2 else nd

        s = self.handle_obs(o)
        s2 = self.handle_obs(o2).detach()

        # Update critic
        for _ in range(self.update_to_data):
            self.update_critic(s, a, r, s2, nd, L, TBL, step)
            self.update_critic_steps += 1
        
        # Update actor
        if step % self.actor_update_freq == 0:
            self.update_actor(s.detach(), L, TBL, step)
            self.update_actor_steps += 1

        # Smooth update
        if step % self.critic_target_update_freq == 0:
            utils.soft_update_params(
                self.critic, self.critic_targ, self.critic_tau
            )
            if self.cpc and self.encoder_type == 'pixel':
                utils.soft_update_params(
                    self.extr.fc, self.extr_targ.fc, self.extr_tau
                )
                utils.soft_update_params(
                    self.extr.convs, self.extr_targ.convs, self.extr_tau
                )

        # Update extractor
        if self.encoder_type == 'pixel':
            s_no_ln = self.extr.forward_no_ln(o)
            if self.sparse:
                self.update_extr(o, L, TBL, step)
                self.update_extr_steps += 1
            else:
                L.store(
                    LossExtrL1=s_no_ln.abs().sum(-1).mean().item(),
                    LossExtrSmoothL1=F.smooth_l1_loss(s_no_ln, torch.zeros_like(s_no_ln)).item(),
                    SparseNum3=-1, SparseNum5=-1
                )
                TBL.log('train_sparse/loss_l1', s_no_ln.abs().sum(-1).mean().item(), step)
                TBL.log('train_sparse/loss_smoothl1', F.smooth_l1_loss(s_no_ln, torch.zeros_like(s_no_ln)).item(), step)
                TBL.log('train_sparse/sparse_num3', -1, step)
                TBL.log('train_sparse/sparse_num5', -1, step)

            if self.cpc:
                self.update_curl(o, step)
                self.update_extr_steps += 1

    def save(self, model_dir, step):
        torch.save(
            self.actor.state_dict(), '%s/actor_%s.pt' % (model_dir, step)
        )
        torch.save(
            self.critic.state_dict(), '%s/critic_%s.pt' % (model_dir, step)
        )
        torch.save(
            self.extr.state_dict(), '%s/extr_%s.pt' % (model_dir, step)
        )
        if self.cpc:
            torch.save(
                self.curl.state_dict(), '%s/curl_%s.pt' % (model_dir, step)
            )
        self._save(model_dir, step)

    @abc.abstractmethod
    def _save(self, model_dir, step):
        pass

    def load(self, model_dir, step):
        self.actor.load_state_dict(
            torch.load('%s/actor_%s.pt' % (model_dir, step))
        )
        self.critic.load_state_dict(
            torch.load('%s/critic_%s.pt' % (model_dir, step))
        )
        self.extr.load_state_dict(
            torch.load('%s/extr_%s.pt' % (model_dir, step))
        )
        if self.cpc:
            self.curl.load_state_dict(
                torch.load('%s/curl_%s.pt' % (model_dir, step))
            )
        self._load(model_dir, step)

    @abc.abstractmethod
    def _load(self, model_dir, step):
        pass
