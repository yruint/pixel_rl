import copy
import torch
import torch.nn.functional as F
import numpy as np

from .agent import BaseAgent
from module.rl_module import SGMLPActor, Critic, EnsembleCritic
from common.utils import update_params, soft_update_params
from common import logger


class SACAgent(BaseAgent):

    def __init__(self, obs_shape, action_shape, action_limit, extr_config,
                 actor_lr=1e-3, alpha_lr=1e-3, actor_update_freq=1, actor_beta=0.9,
                 critic_lr=1e-3, critic_tau=0.005, critic_target_update_freq=1,
                 device='cpu', update_to_data=1, gamma=0.99, hidden_dim=1024,
                 l=2, extr_latent_dim=50, init_temperature=0.1, num_q=2,
                 critic_beta=0.9, actor_log_std_min=-10, actor_log_std_max=2,
                 alpha_beta=0.5, sparse=True, cpc=False, crop=False, **kwargs):
        super().__init__(
            obs_shape, actor_update_freq, critic_tau,
            critic_target_update_freq, update_to_data, device,
            action_limit, gamma, sparse, cpc, crop, extr_config
        )
        # Setting modules
        self.actor = SGMLPActor(
            action_shape, hidden_dim, extr_latent_dim,
            actor_log_std_min, actor_log_std_max, l, self.action_limit
        ).to(device)
        self.critic = EnsembleCritic(
            action_shape, hidden_dim, extr_latent_dim, l, num_q=num_q
        ).to(device)
        self.critic_targ = copy.deepcopy(self.critic)

        self.log_alpha = torch.tensor(np.log(init_temperature)).to(device)
        self.log_alpha.requires_grad = True
        # set target entropy to -|A|
        self.target_entropy = -np.prod(action_shape)
        
        # Setting optimizer
        self.actor_optimizer = torch.optim.Adam(
            self.actor.parameters(), lr=actor_lr, betas=(actor_beta, 0.999))
        self.critic_optimizer = torch.optim.Adam(
            self.critic.parameters(), lr=critic_lr, betas=(critic_beta, 0.999))
        self.extr_q_optimizer = torch.optim.Adam(
            self.extr.parameters(), lr=critic_lr, betas=(critic_beta, 0.999))
        self.log_alpha_optimizer = torch.optim.Adam(
            [self.log_alpha], lr=alpha_lr, betas=(alpha_beta, 0.999))

        self.train()
        self.train_targ()

    @property
    def alpha(self):
        return self.log_alpha.exp()

    def update_critic(self, s, a, r, s2, nd, L, TBL, step):
        q = self.critic(s, a, False) # (num_q, batch_size)
        q_vals = q.min(dim=0)[0].detach()

        with torch.no_grad():
            _, a2, logp_a2, _ = self.actor(s2)
            q_pi_targ = self.critic_targ(s2, a2, False)

            q_targ = r + self.gamma * nd * (q_pi_targ.min(dim=0)[0] - self.alpha * logp_a2)
            q_targ_max = r + self.gamma * nd * (q_pi_targ.max(dim=0)[0] - self.alpha * logp_a2)
        
        loss_q = F.mse_loss(q, q_targ) * q.size(0)
        update_params(
            dict(optim1=self.critic_optimizer, optim2=self.extr_q_optimizer), loss_q
        )

        L.store(Qvals=q.mean().item(), Qmaxs=q.max(dim=0)[0].mean().item(),
                LossQ=loss_q.item(), TQvals=q_targ.mean().item(), TQmaxs=q_targ_max.mean().item())
        TBL.log('train_critic/loss', loss_q.item(), step)
        TBL.log('train_critic/qmin', q.mean().item(), step)
        TBL.log('train_critic/qmax', q.max(dim=0)[0].mean().item(), step)
        TBL.log('train_critic/tqmin', q_targ.mean().item(), step)
        TBL.log('train_critic/tqmax', q_targ_max.mean().item(), step)

    def update_actor(self, s, L, TBL, step):
        _, a, logp_a, log_std = self.actor(s)
        q_pi = self.critic(s, a)

        loss_pi = (self.alpha.detach() * logp_a - q_pi).mean()
        update_params(self.actor_optimizer, loss_pi)

        entropy = 0.5 * log_std.shape[1] * \
            (1.0 + np.log(2 * np.pi)) + log_std.sum(dim=-1)
        
        loss_alpha = (self.alpha * (-logp_a - self.target_entropy).detach()).mean()
        update_params(self.log_alpha_optimizer, loss_alpha)

        L.store(HPi=-logp_a.mean().item(), Entro=entropy.mean().item(),
                LossPi=loss_pi.item(), Alpha=self.alpha.item(), LossAlpha=loss_alpha.item())
        TBL.log('train_actor/loss', loss_pi.item(), step)
        TBL.log('train_actor/loss_alpha', loss_alpha.item(), step)
        TBL.log('train_actor/alpha', self.alpha.item(), step)
        TBL.log('train_actor/hpi', -logp_a.mean().item(), step)
        TBL.log('train_actor/entro', entropy.mean().item(), step)

    def _save(self, model_dir, step):
        pass

    def _load(self, model_dir, step):
        pass
