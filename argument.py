import torch
import argparse


def parse_args():
    parser = argparse.ArgumentParser()
    # environment
    parser.add_argument('--env', type=str, default='dmc.hopper.hop')
    parser.add_argument('--image_size', default=84, type=int)
    parser.add_argument('--pre_image_size', default=100, type=int)
    parser.add_argument('--action_repeat', '-ar', default=4, type=int)
    parser.add_argument('--frame_stack', '-fs', default=3, type=int)
    parser.add_argument('--encoder_type', type=str, default='pixel')

    # algorithm
    parser.add_argument('--alg', type=str, default='sac')
    parser.add_argument('--sparse', default=False, action='store_true')
    parser.add_argument('--cpc', default=False, action='store_true')
    parser.add_argument('--crop', default=False, action='store_true')
    parser.add_argument('--ln', default=True, action='store_true')
    # train
    parser.add_argument('--total_steps', default=5e5, type=int)
    parser.add_argument('--replay_buffer_capacity', default=1e5, type=int)
    parser.add_argument('--batch_size', default=512, type=int)
    parser.add_argument('--hidden_dim', default=1024, type=int)
    parser.add_argument('--steps_per_epoch', default=5000, type=int)
    parser.add_argument('--num_updates', default=1, type=int)
    parser.add_argument('--video_alpha', default=0, type=float)
    # critic
    parser.add_argument('--critic_lr', default=1e-3, type=float)
    parser.add_argument('--critic_target_update_freq', default=2, type=int)
    parser.add_argument('--update_to_data', default=1, type=int)
    # actor
    parser.add_argument('--actor_lr', default=1e-3, type=float)
    parser.add_argument('--actor_update_freq', default=1, type=int)
    # extractor
    parser.add_argument('--extr_lr', default=1e-3, type=float)
    parser.add_argument('--extr_tau', default=0.05, type=float)
    parser.add_argument('--extr_update_freq', default=1, type=int)
    parser.add_argument('--extr_latent_dim', default=50, type=int)
    # sac
    parser.add_argument('--alpha_lr', default=1e-4, type=float)

    # test
    parser.add_argument('--test', default=True, action='store_true')
    parser.add_argument('--num_test_env', '-nte', default=10, type=int)
    parser.add_argument('--num_eval_episodes', default=10, type=int)
    parser.add_argument('--test_video_alpha', default=0.5, type=float)
    # save
    # parser.add_argument('--work_dir', default='.', type=str)
    parser.add_argument('--config_dir', type=str, default='config')
    parser.add_argument('--save_tb', default=True, action='store_true')
    parser.add_argument('--save_buffer', default=False, action='store_true')
    parser.add_argument('--save_video', default=False, action='store_true')
    parser.add_argument('--save_model', default=True, action='store_true')
    # parser.add_argument('--log_interval', default=100, type=int)

    # seed
    parser.add_argument('--seed_list', '-s', nargs='+', type=int,
                        default=[0, 1, 2, 3, 4])
    parser.add_argument('--seed', default=1, type=int)
    # cuda
    parser.add_argument('--no-cuda', action='store_true', default=False)
    parser.add_argument('--cuda_id', type=int, default=0)

    args = parser.parse_args()
    args.total_steps = int(args.total_steps) + 1
    args.replay_buffer_capacity = int(args.replay_buffer_capacity)
    args.eval_freq = args.steps_per_epoch
    args.cuda = not args.no_cuda and torch.cuda.is_available()
    return args
